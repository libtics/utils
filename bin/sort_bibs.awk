#!/usr/bin/gawk -f

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# Use as sort_bibs.awk < input > output
# Sorts the bib entries and glossary entries in ignored case alphabetical

function compare_str_lower_case(i1, v1, i2, v2)
{
    # string value comparison, ascending order
    v1 = tolower(v1) ""
    v2 = tolower(v2) ""
    if (v1 < v2)
        return -1
    return (v1 != v2)
}

BEGIN {
	RS=""; ORS="\n\n"; FS="\n"
	PROCINFO["sorted_in"]="compare_str_lower_case"
}
{
	a[$1]=$0
}

END {
	for(i in a) {
		print a[i]
	}
}
