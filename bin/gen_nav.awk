#!/usr/bin/awk -f

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# fname must be passed as a variable to this awk script
# levels is optional, default to all levels

# get_rest_of_line: return the rest of the awk input line, 'line' i.e. from
# position 'start' to 'nf' (num fields)
function get_rest_of_line(start, nf, line)
{
	rest_of_line = ""
	split(line, a, FS)

	for (i = start; i <= nf; i++) {
		rest_of_line = rest_of_line a[i] " "
	}

	gsub(" *$", "", rest_of_line) # remove trailing spaces
	return rest_of_line
}


# Iterate over the file looking for lines beginning with 'levels' or more '='.
# Use these lines, and possibly the reference (beginning '[#' that would be
# on the previous line (no blank lines) to construct a list of antora cross
# references.

BEGIN {
	prevline = ""
	if (levels)
		level_n="^={1," levels "}?"
	else
		level_n="^=+"
	levels=level_n
	matcher=(levels " [A-Za-z]")
}
{
	if (match($0, matcher) > 0) { # matches a heading, excludes a block
		level = $1
		gsub("=", "*", level)
		if (match(prevline, "^\\[#") > 0) { # It's a xref label
			# use the provided xref
			display_name = get_rest_of_line(2, NF, $0)
			gsub("\\[#|\\]", "", prevline) # extract the current xref
			if (with_in_page_anchors)
				xref = fname "#" prevline
			else
				xref = fname
		} else {
			# construct an xref
			name = get_rest_of_line(2, NF, $0)
			display_name = name
			gsub(" *$", "", display_name) # remove trailing spaces
			gsub(/ -- /, "", name) # asciidoctor ignores hyphen in a xref label
			gsub(/[[:punct:]]/, "", name) # and any punctuation
			gsub(" *$", "", name) # remove trailing spaces
			gsub(" +|^", "_", name) # leading underscore and any spaces
			if (with_in_page_anchors)
				xref = fname "#" tolower(name)
			else
				xref = fname
		}
		print level, "xref:" xref "[" display_name "]"
	}
	prevline = $0
}
END {
}
